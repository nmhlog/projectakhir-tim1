import React, { useEffect, useState } from "react";
import axios from "axios";

export default function useAsync(url) {
  const [data, setData] = useState([]);
  const [isLoading, SetIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      SetIsLoading(true);
      try {
        const { data: response } = await axios.get(url);
        setData(response);
        setIsError(false);
      } catch (error) {
        console.error(error.message);
        setIsError(true);
      } finally {
        if (!isError) {
          SetIsLoading(false);
        }
      }
    };
    fetchData();
  }, []);

  return [data, isLoading, isError];
}
