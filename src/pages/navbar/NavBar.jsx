import React, { useState } from "react";
import { AppBar, Stack, Toolbar } from "@mui/material/";
import { Outlet, useLocation } from "react-router-dom";
import Footer from "../footer/Footer";
import { Grid} from "@mui/material";
import AutoMobile from "../../components/logos/AutoMobile";
import SingupLogin from "../../components/signuplogin/SignupLogin";
import LoginButtonGroup from "../../components/loginbuttongroup/LoginButtonGroup";

export default function NavBar() {
  const { pathname } = useLocation();
  const [isLogin, setIsLogin] = useState(false);
  const heightCondition = pathname.startsWith("/confirm")||pathname.startsWith("/login")||pathname.endsWith("password")?null:"1000px";
  const footerHideCondition = ["/signup", "/login"].indexOf(pathname) >= 0 || pathname.startsWith("/confirm")||pathname.endsWith("password") || pathname.startsWith("/checkout")? null : (
    <Footer />
  )
  return (
    <Stack
      maxWidth={"1280px"}
      minHeight={heightCondition}
      direction={"column"}
      useFlexGap
      justifyContent={"space-between"}
      marginX={"auto"}
    >
      <Grid className="App" maxWidth={"1280px"}>
        <Grid item lg={12}>
          <AppBar
            position="relative"
            sx={{ boxShadow: "none", bgcolor: "white" }}
          >
            <Toolbar
              sx={{
                display: "flex",
                // justifyContent:"space-between"
              }}
            >
              <AutoMobile />

              {pathname!=="/verify" && !isLogin ? (
                <SingupLogin pathname={pathname} />
              ) : null}

              {isLogin ? <LoginButtonGroup /> : null}
            </Toolbar>
          </AppBar>
        </Grid>

        <Outlet />

      </Grid>
      
      {footerHideCondition}
    </Stack>
  );
}
