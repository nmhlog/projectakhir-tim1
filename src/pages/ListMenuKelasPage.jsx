import React from 'react'
import ListCarCardDetailKelasPage from '../components/content/ListCardCardDetailKelasPage';
import BannerListMenuKelas from './../components/BannerListMenuKelas';

const ListMenuKelasPage = () => {
  return (
    <div>
        <BannerListMenuKelas/>
        <ListCarCardDetailKelasPage/>

    </div>
  )
}

export default ListMenuKelasPage