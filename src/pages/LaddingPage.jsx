import React from "react";
import Benefit from "../components/contents/Benefit";
import ImgListType from "../components/contents/ImgListType";
import { Box, Grid } from "@mui/material";
import Benner from "../components/benner/Benner";
// import CarCard from "../components/CarCard";
import ListCarCard from "../components/contents/ListCarCard";
import useAsync from "../hooks/useasync/useAsync";

export default function LaddingPage() {
  const [typeData, isLoadingTypeData, isTypeError]= useAsync("http://localhost:35829/api/TypeProduct/GetTypeProduct");
  const [CarData, isLoadingCarData, isCarError]= useAsync("http://localhost:35829/api/TypeProduct/GetCar");

  return (
    <>
    <Grid container height={"100%"} rowSpacing={"140px"}>
    <Benner />
    <ListCarCard data={CarData} loading={isLoadingCarData} />
    {/* <Box sx={{height:"140px"}}> </Box> */}
    <Benefit />
    {/* <Box sx={{height:"140px"}}> </Box> */}
    <ImgListType typeData={typeData} loading={isLoadingTypeData}/>
    {/* <Box sx={{height:"140px"}}> </Box> */}
    </Grid>
    <Box sx={{height:"140px"}}> </Box>
    </>
  );
}
