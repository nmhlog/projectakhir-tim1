import React from "react";
import CheckoutCard from "../components/CheckoutCard";
import { Modal,Grid } from "@mui/material";
import Payment from "../components/payment/Payment";

const CheckoutPage = () => {
  const [open, setOpen] = React.useState(false);
  const onOpen = ()=>{
    setOpen(true)
  }
  const onClose = ()=>{
    setOpen(false);
  }

  return (
    <Grid>
      <CheckoutCard handleOpen={onOpen}/>
      <Modal
        open={open}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
      <Payment  handleClose={onClose}/>
      </Modal>
     
    </Grid>
  );
};

export default CheckoutPage;
