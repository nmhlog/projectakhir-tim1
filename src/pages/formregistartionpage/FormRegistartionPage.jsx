import React, { useState } from "react";
import FormRegistration from "../../components/formcostum/FormRegistration";
import { Grid, Alert } from "@mui/material";

export default function FormRegistrationContainer() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState(
    "Your password must be have at least 8 characters long one number and one special characters\n"
  );
  const [confirmPassword, setConfirmPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [signUpSuccess, setSignUpSuccess] = useState(false);

  return (
    <>
      <Grid item xs={12}>
        {signUpSuccess ? (
          <Alert
            onClose={() => {
              setSignUpSuccess(false);
            }}
          >
            Sign Up Success.Please Check Your Email For Confirmation
          </Alert>
        ) : null}
        <FormRegistration
          setName={setName}
          name={name}
          email={email}
          setEmail={setEmail}
          password={password}
          setPassword={setPassword}
          passwordError={passwordError}
          setPasswordError={setPasswordError}
          confirmPassword={confirmPassword}
          setConfirmPassword={setConfirmPassword}
          emailError={emailError}
          setEmailError={setEmailError}
          signUpSuccess={signUpSuccess}
          setSignUpSuccess={setSignUpSuccess}
        />
      </Grid>
    </>
  );
}
