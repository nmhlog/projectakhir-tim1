import { Grid, Stack, Divider } from "@mui/material";
import React from "react";
import About from "../../components/about/About";
import ProductList from "../../components/productlist/ProductList";
import AddressContactUs from "../../components/addresscontactus/AddressContactUs";

/*
* TODO:
If media < 900
Footer should be row and centered;

*/ 
export default function Footer() {
  return (
    <>
    
      <Grid item lg={12} >

        <Stack
          direction={"row"}
          useFlexGap
          flexWrap={"wrap"}
          sx={{
            position:"bottom",
            pt:"2%",
            borderTop:1,
            borderTopColor:"#E0E0E0",
            maxHeight: "220px",
            maxWidth: "1280px",
            justifyContent: "space-around",
          }}
        >
          <About />
          <ProductList />
          <AddressContactUs />
        </Stack>
      </Grid>
    </>
  );
}
