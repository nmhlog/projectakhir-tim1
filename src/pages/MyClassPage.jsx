import React from 'react'
import MyClassCard from '../components/MyClassCard'
import Footer from './footer/Footer';

const MyClassPage = () => {
  return (
    <div>
        <MyClassCard/>
        <Footer/>
    </div>
  )
}

export default MyClassPage