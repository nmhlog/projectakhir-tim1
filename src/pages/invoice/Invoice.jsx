import React from "react";
import InvoiceTable from "../../components/invoicetable/InvoiceTable";
import { Grid, Typography } from "@mui/material";
import { NavLink } from "react-router-dom";
import BreadCrumbsCore from "../../components/breadcrumbs/BreadCrumbsCore";

export default function Invoice() {
  return (
    <>
      <Grid item lg={12} marginX={"70px"}>
        {/* <Typography
          sx={{
            color: "#828282",
            fontFamily: '"Montserrat",sans-serif',
            fontSize: "16px",
            fontStyle: "normal",
            fontWeight: 600,
            lineHeight: "19.5px",
            mb: "2%",
          }}
        >
          Home &gt; <span style={{ color: "#790B0A" }}>Invoice</span>
        </Typography> */}
        < BreadCrumbsCore listDir={[["Home","/"]]} currentDir={"Invoice"} />
        <Typography
          sx={{
            color: "#4F4F4F",
            fontFamily: '"Montserrat",sans-serif',
            fontSize: "20px",
            fontStyle: "normal",
            fontWeight: 600,
            lineHeight: "24.38px",
            mb: "2%",
          }}
        >
          Menu Invoice
        </Typography>

        <InvoiceTable />

      </Grid>
    </>
  );
}
