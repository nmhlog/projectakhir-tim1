import React from "react";
import InvoiceTableDetail from "../../components/invoicetable/InvoiceTableDetail";
import { Grid, Typography } from "@mui/material";
import BreadCrumbsCore from "../../components/breadcrumbs/BreadCrumbsCore";
import { Table, TableRow } from "@mui/material";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { HeaderCellInvoice } from "../../components/invoicetable/StyledTable";

export default function InvoiceDetail() {
  return (
    <>
      <Grid item lg={12} marginX={"70px"}>
        <BreadCrumbsCore
          listDir={[
            ["Home", "/"],
            ["Invoice", "/Invoice"],
          ]}
          currentDir={"Details Invoice"}
        />

        <Typography
          sx={{
            color: "#4F4F4F",
            fontFamily: '"Montserrat",sans-serif',
            fontSize: "20px",
            fontStyle: "normal",
            fontWeight: 600,
            lineHeight: "24.38px",
            mb: "2%",
          }}
        >
          Details Invoice
        </Typography>
        <Table
          sx={{
            minWidth: 700,
            [`& .${tableCellClasses.root}`]: {
              borderBottom: "none",
            },
          }}
        >
          <TableRow
            sx={{
              fontFamily: '"Montserrat",sans-serif',
              fontWeight: 500,
              fontSize: "18px",
              lineHeight: "22px",
            }}
          >
            <HeaderCellInvoice align="left" sx={{ width: "15%" }}>
              No. Invoice{" "}
            </HeaderCellInvoice>
            <HeaderCellInvoice align="left">: OTO00003</HeaderCellInvoice>
          </TableRow>
          <TableRow>
            <HeaderCellInvoice align="left" sx={{ width: "15%", pt: "0%" }}>
              Date{" "}
            </HeaderCellInvoice>
            <HeaderCellInvoice align="left" sx={{ pt: "0%" }}>
              : 12 June 2022
            </HeaderCellInvoice>
            <TableCell
              align="right"
              sx={{
                fontFamily: '"Montserrat",sans-serif',
                fontWeight: 700,
                fontSize: "18px",
                lineHeight: "22px",
              }}
            >
              Total Price :
            </TableCell>
            <TableCell
              align="right"
              sx={{
                fontFamily: '"Montserrat",sans-serif',
                fontWeight: 700,
                fontSize: "18px",
                lineHeight: "22px",
              }}
            >
              IDR 850.000
            </TableCell>
          </TableRow>
        </Table>
        <InvoiceTableDetail />
      </Grid>
    </>
  );
}
