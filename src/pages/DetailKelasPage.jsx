import React from 'react'
import BannerDetailKelas from '../components/BannerDetailKelas'
// import ListCarCardDetailKelasPage from '../components/content/ListCardCardDetailKelasPage';
import ListCarCardDetailKelasPage from '../components/contents/ListCardCardDetailKelasPage';
import Footer from './footer/Footer';

const DetailKelasPage = () => {
  return (
    <div>
        <BannerDetailKelas/>
        <ListCarCardDetailKelasPage/>
    </div>
  )
}

export default DetailKelasPage