import React, { useEffect, useState } from "react";
import ConfirmSuccess from "../../components/confirm/ConfirmSuccess";
import { useSearchParams } from "react-router-dom";
import axios from "axios";



export default function VerifySignUp(){
    const [searchParams] = useSearchParams(); 
    const [success,setSuccess] = useState(false);
    const token = searchParams.get("token");
    const config = {
        headers: { Authorization: `Bearer ${token}` }
      };
    useEffect(()=>{
            axios.get('http://localhost:35829/api/register/ValidateToken',config)
            .then(res=>setSuccess(true))
            .catch(e=>console.error(e));
    },[])

    return(<>
    {(success)?<ConfirmSuccess />:null}
    
    </>)
}