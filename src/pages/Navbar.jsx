import React from "react";
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import SingupLogin from "../components/SingupLogin";
import { NavLink, Outlet, useLocation } from "react-router-dom";
import {Grid, Typography} from "@mui/material";
import AutoMobile from "../components/logos/AutoMobile";
import { keluarAplikasi, auth } from "../authentication/Firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';
import {Box} from "@mui/material";

export default function NavBar(){
    //const {pathname} = useLocation();
    //const navigate = useNavigate();
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [user, loading] = useAuthState(auth);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const buttonLogoutOnClickHandler = async () => {
    await keluarAplikasi();
    //navigate("/");
  };
    return (
      <Grid  className="App" >
            <AppBar position="relative" sx={{boxShadow:"none",bgcolor:"white"}}>
              <Toolbar sx={{
                display:"flex",
                // justifyContent:"space-between"
              }}>
                <AutoMobile/>
                {loading ? null : user ? (
            <Box sx={{ flexGrow: 0 }}>
              <ShoppingCartIcon/>
              <Typography>My Class</Typography>
              <Typography>Invoice</Typography>
              <PersonIcon/>
              <LogoutIcon/>
            </Box>
          ) : (
           <SingupLogin/>
          )}   
              </Toolbar>
            </AppBar>
        <Outlet />
      </Grid>
    )
}
