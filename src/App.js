import React from "react"
import FormRegistartionPage from "./pages/formregistartionpage/FormRegistartionPage";
import LaddingPage from "./pages/LaddingPage";
import { Route,Routes } from "react-router-dom";
import NavBar from "./pages/navbar/NavBar";
import Invoice from "./pages/invoice/Invoice";
import InvoiceDetail from "./pages/invoice/InvoiceDetail";
import VerifySignUp from "./pages/verify/VerifySignup";
import ConfirmPurchase from "./components/confirm/ConfirmPurchase";
import LoginCard from "./components/LoginCard";
import ResetPwCard from "./components/ResetPwCard";
import EmailResetPwCard from "./components/EmailResetPwCard"
import Payment from "./components/payment/Payment";
import CheckoutPage from "./pages/CheckoutPage"
// import DetailKelasPage from "./pages/DetailKelasPage"
// import ListMenuKelasPage from "./pages/ListMenuKelasPage";

function App() {
  return (
   <Routes>
      <Route path="/" element={<NavBar/>} >
        <Route index element={<LaddingPage />} />
        <Route path="/signup" element={<FormRegistartionPage />} />
        <Route path="/resetpassword" element={<ResetPwCard />} />
        <Route path="/forgetpassword" element={<EmailResetPwCard />} />
        <Route path="/login" element={<LoginCard />} />
        <Route path="/confirmsignup" element={<VerifySignUp />} />
        <Route path="/invoice" element={<Invoice />} />
        <Route path="/detail" element={<InvoiceDetail />} />
        <Route path="/confirmpurchase" element={<ConfirmPurchase />} />
        <Route path="/payment" element={<Payment />} />
        <Route path="/checkout" element={<CheckoutPage />} />
        {/* <Route path="/DetailKelasPage" element={<DetailKelasPage />} /> */}
        {/* <Route path="/ListMenuKelasPage" element={<ListMenuKelasPage />} /> */}




        
      </Route>
   </Routes>

  );
}

export default App;
