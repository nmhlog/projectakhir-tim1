import React from "react";
import { Box, Stack, Typography, Grid } from "@mui/material";

export default function Benner() {
  return (
    <>
      <Grid item lg={12} sx={{mx:"auto",width:"100%"}}>
        <Box
          sx={{
            backgroundImage: 'url("http://localhost:3000/Image/Banner.png")',
            height: "496px",
            
          }}
        >
          <Box >
            <Stack sx={{pt:"120px"}} gap={2}>
              <Typography variant="h2" color="#FFFFFF" fontSize={32} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"600",
                lineHeight:"39.01px"
                }}>
                We provide driving lessons for various types of cars

              </Typography>
              <Typography variant="h2" color="#FFFFFF" fontSize={24} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"400",
                lineHeight:"29.26px"
                }}>
                Professional staff who are ready to help you to become a
                much-needed reliable driver
              </Typography>
            </Stack>
            <Stack flexDirection={"row"} gap={2} sx={{justifyContent: 'center' ,pt:"73px"}}>
              <Box sx={{width:324,height:207}}>
                <Typography variant="h2" color="#FFFFFF" fontSize={48} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"600px",
                lineHeight:"58.51px"
                }}>
                  50+
                </Typography>
                <Typography variant="h2" color="#FFFFFF" fontSize={20} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"500px",
                lineHeight:"19.5px"
                }}>
                  A class ready to make you a reliable driver
                </Typography>
              </Box>
              <Box sx={{width:324,height:207}}>
                <Typography variant="h2" color="#FFFFFF" fontSize={48} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"600px",
                lineHeight:"58.51px"
                }}>
                  20+
                </Typography>
                <Typography variant="h2" color="#FFFFFF" fontSize={20} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"500px",
                lineHeight:"19.5px"
                }}>
                  Professional workforce with great experience
                </Typography>
              </Box>
              <Box sx={{width:324,height:207}}>
                <Typography variant="h2" color="#FFFFFF" fontSize={48} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"600px",
                lineHeight:"58.51px"
                }}>
                  10+
                </Typography>
                <Typography variant="h2" color="#FFFFFF" fontSize={20} sx={{
                textAlign:"center",
                fontFamily:"'Montserrat',sans-serif",
                fontWeight:"500px",
                lineHeight:"19.5px"
                }}>
                  Cooperate with driver service partners
                </Typography>
              </Box>
            </Stack>
          </Box>
        </Box>
      </Grid>
    </>
  );
}
