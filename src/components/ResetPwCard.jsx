import React, { useState } from 'react';
import { Card, CardContent, Typography, TextField, Button, Box } from '@mui/material';

const ResetPwCard = () => {
  const [newPw, setNewPw] = useState('');
  const [confNewPw, setConfNewPw] = useState('');

 

  return (
    <div style={{display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh'}} >
    <Card style={{width: '50%'}}>
      <CardContent>
        <Box display='flex' justifyContent='flex-start' marginBottom='30px'>
        <Typography variant='h4' color='grey' >Create Password</Typography>
        </Box>
        <form>
          <TextField
            label="New Password"
            variant="outlined"
            type="password"
            fullWidth
            value={newPw}
            onChange={(e) => setNewPw(e.target.value)}
            margin="normal"
          />
          <TextField
            label="Confirm New Password"
            variant="outlined"
            fullWidth
            value={confNewPw}
            onChange={(e) => setConfNewPw(e.target.value)}
            type="password"
            margin="normal"
          />
          
        <Box  display='flex' justifyContent='flex-end' marginTop='30px'>
            <Button  variant='contained' style={{ marginRight:'20px', backgroundColor: '#fff', color: '#790B0A',width:'10%'}} >
            Cancel
          </Button>
          <Button  variant='contained' style={{ backgroundColor: '#790B0A', color: '#fff',width:'10%'}} >
            Login
          </Button>
          </Box>
        </form>
      </CardContent>
    </Card>
    </div>
  );
};

export default ResetPwCard;