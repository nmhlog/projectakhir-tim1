import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material';
const BannerListMenuKelas = () => {
    const loremText = `
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed luctus euismod dui, ac placerat velit fermentum eget.
    Curabitur in mi eget ex lacinia varius vel vitae lectus. Integer sed felis non tortor vehicula rhoncus vel in quam.
    Praesent tincidunt turpis vitae elit euismod, vel eleifend erat hendrerit.
  `;
  return (
    <div>
    <Card style={{ width: '100%' }}>
    <CardMedia
        sx={{ width:'100%' ,height: 500 }}
        image="http://placekitten.com/1600"
        title="SUV"
      />
      <CardContent>
      <Box display='flex' justifyContent='flex-start' flexDirection='column' alignItems='left' margin='100px'>
        <Typography variant="h2" align="left" marginBottom='20px'>
          SUV
        </Typography>
        <Typography gutterBottom variant="h4" color="text.secondary" align="left" >
          {loremText}
        </Typography>
        </Box>
      </CardContent>
    </Card>
    </div>
  )
}

export default BannerListMenuKelas