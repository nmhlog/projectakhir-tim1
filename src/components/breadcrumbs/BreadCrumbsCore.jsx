import React from "react";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Typography from "@mui/material/Typography";
import { NavLink } from "react-router-dom";
import { styled } from "@mui/system";


const StyledNavLink = styled(NavLink)(() => ({
  color: "#828282",
  fontFamily: '"Montserrat",sans-serif',
  fontSize: "16px",
  fontStyle: "normal",
  fontWeight: 600,
  lineHeight: "20px",
  textDecoration: "none",
}));

export default function BreadCrumbsCore({ listDir, currentDir }) {
  return (
    <>
      <Breadcrumbs separator={">"} sx={{ mb: "2%" }}>
        {listDir.map((e) => (
          <StyledNavLink to={e[1]}>{e[0]}</StyledNavLink>
        ))}
        <Typography
          sx={{
            color: "#790B0A",
            fontFamily: '"Montserrat",sans-serif',
            fontSize: "16px",
            fontStyle: "normal",
            fontWeight: 600,
            lineHeight: "20px",
            textDecoration: "none",
          }}
        >
          {currentDir}
        </Typography>
      </Breadcrumbs>
    </>
  );
}
