import React, { useState } from 'react';
import { Card, CardContent, Typography, TextField, Button, Box } from '@mui/material';

const EmailResetPwCard = () => {
    const [email, setEmail] = useState('');

 

  return (
    <div style={{display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh'}} >
    <Card style={{width: '50%'}}>
      <CardContent>
        
        <Box  display='flex' justifyContent='flex-start'>
        <Typography variant='h3' color='grey' >Reset Password</Typography>
        </Box>
        <Box display='flex' justifyContent='flex-start' marginBottom='30px'>
        <Typography variant='h5' color='grey' >Send OTP Code to your email address</Typography>
        </Box>
        <form>
        <TextField
            label="Email"
            variant="outlined"
            fullWidth
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            margin="normal"
          />
          
        <Box  display='flex' justifyContent='flex-end' marginTop='30px'>
            <Button  variant='contained' style={{ marginRight:'20px', backgroundColor: '#fff', color: '#790B0A',width:'10%'}} >
            Cancel
          </Button>
          <Button  variant='contained' style={{ backgroundColor: '#790B0A', color: '#fff',width:'10%'}} >
            Login
          </Button>
          </Box>
        </form>
      </CardContent>
    </Card>
    </div>
  );
};

export default EmailResetPwCard;