import React from "react";
// import Box from "@mui/material/Box";

import {
  Stack,
  TextField,
  Box,
  Button,
  FormLabel,
  ThemeProvider,
  createTheme,
  Alert,
  AlertTitle,
} from "@mui/material";
import { NavLink } from "react-router-dom";
import TextGroup from "../textcostum/TextGroup";
import { useNavigate } from "react-router-dom";
import axios from "axios";

export default function FormRegistration(props) {
  const {
    setName,
    name,
    email,
    setEmail,
    password,
    setPassword,
    passwordError,
    setPasswordError,
    confirmPassword,
    setConfirmPassword,
    emailError,
    setEmailError,
    setSignUpSuccess,
  } = props;
  const theme = createTheme({
    typography: {
      fontFamily: ["Montserrat", "sans-serif"].join(","),
    },
  });
  const navigate = useNavigate();
  const validatePassWord = (val) => {
    if (
      val.length > 6 &&
      /[a-z]/g.test(val) &&
      /[A-Z]/g.test(val) &&
      /[0-9]/g.test(val) &&
      /[!@#$%^&*]/g.test(val)
    ) {
      setPasswordError("");
      return;
    }
    setPasswordError(
      "Your password must be have at least 8 characters long one number and one special characters\n"
    );

    return;
  };

  const EmailErrorTimeOut = () => {
    setTimeout(() => {
      setEmailError("");
    }, 10000);
  };

  const navigateTo= ()=>{
    setTimeout(()=>{
      navigate("/")
    },20000)
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    let data = {
      name: name,
      email: email,
      password: password,
    };
    axios
      .post("http://localhost:35829/api/register/RegisterUser", data)
      .then((response) => {
        console.log(response);
        setSignUpSuccess(true)
        // navigateTo();
      })
      .catch((error) => {
        if (error.response.status === 409) {
          setEmailError(error.response.data.message);
          EmailErrorTimeOut();
          console.log("work");
        }
      });

  };
  return (
    <>
      <ThemeProvider theme={theme}>
        <Box
          sx={{
            height: "480px",
            width: "40%",
            mx: "auto",
            // overflow:'hidden'
          }}
        >
          <form onSubmit={handleSubmit}>
            <Stack spacing={2}>
              <TextGroup
                mainName={"Lets Join our couse!"}
                SupportName={"Please register first"}
                textAlign="Left"
              />
              <TextField
                // id="outlined-input-require"
                required
                variant="outlined"
                size="small"
                label="Name"
                type="Text"
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <TextField
                // id="outlined-input-require"
                required
                error={emailError}
                variant="outlined"
                size="small"
                label="Email"
                type="email"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
              {(emailError)?(<Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                {emailError}
              </Alert>):null}
              <TextField
                // id="outlined-input-require"
                error={passwordError}
                helperText={passwordError}
                required
                variant="outlined"
                size="small"
                label="Password"
                type="password"
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                  validatePassWord(e.target.value);
                }}
              />
              <TextField
                // id="outlined-input-require"
                error={confirmPassword !== password}
                helperText={
                  confirmPassword !== password ? "Password Must Be Same" : null
                }
                required
                variant="outlined"
                size="small"
                label="Confirm Password"
                type="password"
                value={confirmPassword}
                onChange={(e) => {
                  setConfirmPassword(e.target.value);
                }}
              />
              <Box
                sx={{
                  pt: 2,
                  pb: 0,
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <Button
                  variant="contained"
                  size="small"
                  type="submit"
                  sx={{
                    px: "20px",
                    py: "5px",
                    color: "white",
                    bgcolor: "#790B0A",
                    textTransform: "none",
                    ":hover": {
                      color: "#790B0A",
                      bgcolor: "white",
                    },
                    borderRadius: "10px",
                  }}
                >
                  Sign Up
                </Button>
              </Box>

              <FormLabel sx={{ textAlign: "center", my: 0 }}>
                Have account? <NavLink to={"/login"}>Login here</NavLink> <></>
              </FormLabel>
            </Stack>
          </form>
        </Box>
      </ThemeProvider>
    </>
  );
}
