import { Box, Stack, Typography } from "@mui/material";
import React from "react";
// font-family: 'Poppins', sans-serif;
export default function About(porps) {
  return (
    <Stack
      direction={"column"}
      spacing={2}
      sx={{
        width: "30%",
      }}
    >
      <Box>
        <Typography
          variant="h6"
          color="#790B0A"
          sx={{
            fontFamily: "'Poppins', sans-serif",
            fontWeight: "500",
            size: "16px",
            lineHeight: "24px",
            textAlign: "justify",
          }}
        >
          About
        </Typography>
      </Box>
      <Box>
        <Typography
          variant="p"
          color="#333333"
          sx={{
            fontFamily: "'Poppins', sans-serif",
            fontWeight: "400",
            size: "14px",
            lineHeight: "12px",
            textAlign: "justify",
          }}
        >
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Delectus
          deleniti impedit beatae nulla sunt, quos officiis accusantium,
          assumenda architecto eaque officia. Porro nemo nostrum odio repellat
          vero deleniti molestiae totam?
        </Typography>
      </Box>
      {/* </Stack> */}
    </Stack>
  );
}
