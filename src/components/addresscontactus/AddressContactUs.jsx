import React from "react";
import { Box, Stack, Typography, Avatar } from "@mui/material";
import { ReactComponent as PaperPlane } from "./paperplane.svg";
import InstagramIcon from "@mui/icons-material/Instagram";
import MailOutlinedIcon from "@mui/icons-material/MailOutlined";
import YouTubeIcon from "@mui/icons-material/YouTube";
import PhoneIcon from "@mui/icons-material/Phone";
import SvgIcon from "@mui/material/SvgIcon";

export default function AddressContactUs() {
  return (
    <Stack flexDirection={"column"} gap={2} sx={{width:"30%"}}>
      <Stack gap={2}>
        <Box>
          <Typography
            variant="h6"
            color="#790B0A"
            sx={{
              fontFamily: "'Poppins', sans-serif",
              fontWeight: "500",
              size: "16px",
              lineHeight: "24px",
              textAlign: "justify",
            }}
          >
            Address
          </Typography>
        </Box>
        <Box>
          <Typography
            variant="p"
            color="#333333"
            sx={{
              fontFamily: "'Poppins', sans-serif",
              fontWeight: "400",
              size: "14px",
              lineHeight: "12px",
              textAlign: "justify",
            }}
          >
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque.
          </Typography>
        </Box>
      </Stack>
      <Stack gap={2}>
        <Box>
          <Typography
            variant="h6"
            color="#790B0A"
            sx={{
              fontFamily: "'Poppins', sans-serif",
              fontWeight: "500",
              size: "16px",
              lineHeight: "24px",
              textAlign: "justify",
            }}
          >
            Contact Us
          </Typography>
        </Box>
        <Stack flexDirection={"row"} gap={2}>
          <Avatar sx={{ bgcolor: "#790B0A" }}>
            <PhoneIcon />
          </Avatar>

          <Avatar sx={{ bgcolor: "#790B0A" }}>
            <InstagramIcon />
          </Avatar>
          <Avatar sx={{ bgcolor: "#790B0A" }}>
            <YouTubeIcon />
          </Avatar>
          <Avatar sx={{ bgcolor: "#790B0A" }}>
            <SvgIcon>
              <PaperPlane />
            </SvgIcon>
          </Avatar>
          <Avatar sx={{ bgcolor: "#790B0A" }}>
            <MailOutlinedIcon />
          </Avatar>
        </Stack>
      </Stack>
    </Stack>
  );
}
