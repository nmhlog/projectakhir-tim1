import { Stack, Box, Typography } from "@mui/material";
import imgBankMandiri from "./BankMandiri.png";

export default function BankMandiri() {
  return (
    <Stack direction={"row"} alignItems={"center"} gap={2}>
      <Box>
        <img src={imgBankMandiri} alt="Gopay" />
      </Box>
      <Typography
        sx={{
          color: "var(--new-neutral-n-20, #41454D)",
          fontFamily: "Poppins",
          fontSize: "18px",
          fontStyle: "normal",
          fontWeight: 500,
          lineHeight: "normal",
        }}
      >
        BANK MANDIRI
      </Typography>
    </Stack>
  );
}
