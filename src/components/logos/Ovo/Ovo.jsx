import { Stack, Box, Typography } from "@mui/material";
import imgOvo from "./Ovo.png";

export default function Ovo() {
  return (
    <Stack direction={"row"} alignItems={"center"} gap={2}>
      <Box>
        <img src={imgOvo} alt="Gopay" />
      </Box>
      <Typography
        sx={{
          color: "var(--new-neutral-n-20, #41454D)",
          fontFamily: "Poppins",
          fontSize: "18px",
          fontStyle: "normal",
          fontWeight: 500,
          lineHeight: "normal",
        }}
      >
        OVO
      </Typography>
    </Stack>
  );
}
