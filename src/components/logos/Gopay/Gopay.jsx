import { Box, Stack,Typography} from "@mui/material";
import imgGopay from "./gopay.png";


export default function Gopay() {
  return (
    <Stack direction={"row"} alignItems={"center"} gap={2}>
      <Box>
        <img src={imgGopay} alt="Gopay" />
      </Box>
      <Typography
        sx={{
          color: "var(--new-neutral-n-20, #41454D)",
          fontFamily: "Poppins",
          fontSize: "18px",
          fontStyle: "normal",
          fontWeight: 500,
          lineHeight: "normal",
        }}
      >
        GOPAY
        </Typography>
    </Stack>
  );
}
