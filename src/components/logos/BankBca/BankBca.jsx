import { Stack, Box, Typography } from "@mui/material";
import imgBankBca from "./BankBca.png";

export default function BankBca() {
  return (
    <Stack direction={"row"} alignItems={"center"} gap={2}>
      <Box>
        <img src={imgBankBca} alt="Gopay" />
      </Box>
      <Typography
        sx={{
          color: "var(--new-neutral-n-20, #41454D)",
          fontFamily: "Poppins",
          fontSize: "18px",
          fontStyle: "normal",
          fontWeight: 500,
          lineHeight: "normal",
        }}
      >
        BANK BCA
      </Typography>
    </Stack>
  );
}
