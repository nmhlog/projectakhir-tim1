import { Stack, Box, Typography } from "@mui/material";
import imgBankBni from "./BankBni.png";

export default function BankBni() {
  return (
    <Stack direction={"row"} alignItems={"center"} gap={2}>
      <Box>
        <img src={imgBankBni} alt="Gopay" />
      </Box>
      <Typography
        sx={{
          color: "var(--new-neutral-n-20, #41454D)",
          fontFamily: "Poppins",
          fontSize: "18px",
          fontStyle: "normal",
          fontWeight: 500,
          lineHeight: "normal",
        }}
      >
        BANK BNI
      </Typography>
    </Stack>
  );
}
