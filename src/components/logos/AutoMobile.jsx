import React from "react";
import { Box, Typography } from "@mui/material";
import { ReactComponent as Logo } from "./automobil.svg";
import { NavLink } from "react-router-dom";

export default function AutoMobile() {
  return (
    <Box
      sx={{
        display: "flex",
        flexFlow: "row",
        flexGrow: 1,
        justifyContent: "flex-start",
        alignItems: "center",
      }}
    >
      <NavLink to={"/"} sx={{ textDecoration: "none" }}>
        <Box>
          <Logo style={{ width: 63, height: 50 }} />
        </Box>
      </NavLink>
      <Typography
        variant="h6"
        sx={{
          color: "black",
          fontFamily: '"Montserrat",sans-serif',
          fontSize: "24px",
          fontStyle: "normal",
          fontWeight: 400,
          lineHeight: "normal",
          textDecoration: "none",
        }}
      >
        Otomobil
      </Typography>
    </Box>
  );
}
