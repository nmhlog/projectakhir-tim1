import { Stack, Box, Typography } from "@mui/material";
import imgDana from "./Dana.png";

export default function Dana() {
  return (
    <Stack direction={"row"} alignItems={"center"} gap={2}>
      <Box>
        <img src={imgDana} alt="Gopay" />
      </Box>
      <Typography
        sx={{
          color: "var(--new-neutral-n-20, #41454D)",
          fontFamily: "Poppins",
          fontSize: "18px",
          fontStyle: "normal",
          fontWeight: 500,
          lineHeight: "normal",
        }}
      >
        DANA
      </Typography>
    </Stack>
  );
}
