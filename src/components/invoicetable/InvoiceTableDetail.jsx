import React from "react";
import {
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { Paper } from "@mui/material";
import { StyledTableCell,StyledTableRow } from './StyledTable'

function createData(CourseName, typeCourse, Schedule, price) {
  return { CourseName, typeCourse, Schedule, price };
}
const rows = [
  createData("Hyundai Palisade 2021","SUV", "Wednesday, 27 July 2022", "IDR 850.000"),
  createData("Hyundai Palisade 2021","SUV",  "Wednesday, 27 July 2022","IDR 850.000"),
  createData( "Hyundai Palisade 2021", "SUV", "Wednesday, 27 July 2022","IDR 850.000"),
];

export default function InvoiceTableDetail(){


    return (
        <>
        <TableContainer component={Paper} sx={{ borderRadius: 0 }}>
        <Table>
          <TableHead>
            <TableRow>
              <StyledTableCell>No.</StyledTableCell>
              <StyledTableCell>Course Name</StyledTableCell>
              <StyledTableCell>Type</StyledTableCell>
              <StyledTableCell>Schedule</StyledTableCell>
              <StyledTableCell>Price</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row,index) => (
              <StyledTableRow key={index+1}>
                <StyledTableCell>{index+1}</StyledTableCell>
                <StyledTableCell>{row.CourseName}</StyledTableCell>
                <StyledTableCell>{row.typeCourse}</StyledTableCell>
                <StyledTableCell>{row.Schedule}</StyledTableCell>
                <StyledTableCell>{row.price}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
        </>
    );
}