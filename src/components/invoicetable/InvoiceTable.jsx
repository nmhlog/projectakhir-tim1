import React from "react";
import {
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { Paper } from "@mui/material";
import ButtonCore from "../buttoncustom/ButtonCore";
import { StyledTableCell,StyledTableRow } from './StyledTable'

function createData(no, noInvoice, date, totalCourse, totalPrice) {
  return { no, noInvoice, date, totalCourse, totalPrice };
}
const rows = [
  createData(1, "OTO0003", "12 July 2022", 1, "IDR 850.000"),
  createData(2, "OTO0002", "05 Februari 2022", 1, "IDR 850.000"),
  createData(3, "OTO0001", "30 Augustus 2021", 3, "IDR 850.000"),
];

export default function InvoiceTable() {
  return (
    <>
      <TableContainer component={Paper} sx={{ borderRadius: 0 }}>
        <Table>
          <TableHead>
            <TableRow>
              <StyledTableCell>No.</StyledTableCell>
              <StyledTableCell>No. Invoice</StyledTableCell>
              <StyledTableCell>Date</StyledTableCell>
              <StyledTableCell>Total Course</StyledTableCell>
              <StyledTableCell>Total Price</StyledTableCell>
              <StyledTableCell>Action</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <StyledTableRow key={row.no}>
                <StyledTableCell>{row.no}</StyledTableCell>
                <StyledTableCell>{row.noInvoice}</StyledTableCell>
                <StyledTableCell>{row.date}</StyledTableCell>
                <StyledTableCell>{row.totalCourse}</StyledTableCell>
                <StyledTableCell>{row.totalPrice}</StyledTableCell>
                <StyledTableCell>
                  {
                    <ButtonCore
                      NavTo={"/"}
                      name="Details"
                      isReverse={true}
                      isBorder={true}
                    />
                  }
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
