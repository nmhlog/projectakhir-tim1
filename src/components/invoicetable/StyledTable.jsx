import TableCell,{tableCellClasses} from "@mui/material/TableCell";
import { styled } from "@mui/material";
import {TableRow} from "@mui/material";
const StyledTableCell = styled(TableCell)(() => ({
    color: "#4F4F4F",
    fontFamily: '"Montserrat",sans-serif',
    fontSize: "16px",
    fontStyle: "normal",
    lineHeight: "20px",
    textAlign:"center",
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: "#790B0A",
      color: "white",
      fontWeight: 700,

    },
    [`&.${tableCellClasses.body}`]: {
      fontWeight: 500,
    },
  }));
  
  const StyledTableRow = styled(TableRow)(() => ({
    '&:nth-of-type(even)': {
      backgroundColor: "rgba(121, 11, 10,0.1)",
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const HeaderCellInvoice = styled(TableCell)(()=>({
    color: "#4F4F4F",
    fontFamily: '"Montserrat",sans-serif',
    fontSize: "18px",
    fontStyle: "normal",
    lineHeight: "22px",
  }))

export {StyledTableCell,StyledTableRow,HeaderCellInvoice};
//   module.exports.StyledTableCell = StyledTableCell;
//   module.exports.StyledTableRow = StyledTableRow;

