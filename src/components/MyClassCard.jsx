import { Typography } from '@mui/material'
import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { Box } from '@mui/material';
const MyClassCard = () => {
  const MyClassItems=
    [
      { id: 1, img:'http://placekitten.com/1600',title: 'SUV', name:'Hyundai Palisade 2021', schedule:'Wednesday,27 July 2021'},
      { id: 2, img:'http://placekitten.com/1600', title: 'SUV',name:'Toyota Fortunner', schedule:'Sunday,24 July 2021'},
      { id: 3, img:'http://placekitten.com/1600', title: 'SUV',name:'Course Suzuki XL7', schedule:'Wednesday,27 July 2021'},
    ]
  
  return (
    <div>
      {MyClassItems.map((item)=>
        <Card style={{ width: '100%'  }} >
      <Box width='100%' display='flex' flexDirection='row' paddingLeft='100px' paddingRight='100px' paddingTop='20px' paddingBottom='20px'>
        
    <CardMedia
        sx={{ width:200 ,height: 200 }}
        image={item.img}
        title={item.title}
      />
      <CardContent>
      <Box display='flex' justifyContent='flex-start' flexDirection='column' alignItems='left' marginLeft='50px' width='100%' >
      <Typography gutterBottom variant="h5" color="text.secondary" align="left" >
          {item.title}
        </Typography>
        <Typography gutterBottom variant="h4" align="left">
          {item.name}
        </Typography>
        <Box display='flex'>
        <Typography variant="h4" color="#790B0A"  align="left">
          Schedule : 
        </Typography>
        <Typography variant="h4" color="#790B0A"  align="left"> {item.schedule}</Typography>
        </Box>
        
    </Box>
    </CardContent>
    </Box>
    </Card>
    )}
    </div>
  )
}

export default MyClassCard