import React from "react";
import { Modal, Box, Stack, Typography } from "@mui/material";
import Gopay from "../logos/Gopay/Gopay";
import Ovo from "../logos/Ovo/Ovo";
import Dana from "../logos/Dana/Dana";
import BankMandiri from "../logos/BankMandiri/BankMandiri";
import BankBca from "../logos/BankBca/BankBca";
import BankBni from "../logos/BankBni/BankBni";
import ButtonCore from "../buttoncustom/ButtonCore";
const style = {
  display: "flex",
  mx:"auto",
  width: "30%",
  padding: "2%",
  flexDirection: "column",
  borderRadius: "10px",
  border:"2px solid black",
  background: "var(--white, #FFF)",
  gap: "32px",
};

export default function Payment(props) {
  const {handleClose} = props
  return (
    <Box sx={{ ...style }}>
      <Typography
        sx={{
          color: "var(--new-neutral-n-20, #41454D)",
          ml: "20%",
          mr: "10%",
          fontFamily: "Poppins",
          fontSize: "20px",
          fontStyle: "normal",
          fontWeight: 500,
          lineHeight: "normal",
          textAign: "center",
        }}
      >
        Select Payment Method
      </Typography>
      <Stack direction={"column"} gap={2} sx={{ mr: "10%" }}>
        <Gopay />
        <Ovo />
        <Dana />
        <BankMandiri />
        <BankBca />
        <BankBni />
      </Stack>
      <Stack
        direction={"row"}
        gap={2}
        justifyContent={"space-around"}
      >
        <ButtonCore isReverse={true} isBorder={true} size={"Large"} pxsize={"16px"} pysize={"12px"} onclick={handleClose}>
          Cancel
        </ButtonCore>

        <ButtonCore NavTo={"/confirmpurchase"} pxsize={"16px"} pysize={"12px"}> Pay Now </ButtonCore>
      </Stack>
    </Box>
  );
}
