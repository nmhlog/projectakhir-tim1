import React from "react";
import { Grid, Typography } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { Box, Button } from "@mui/material";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { useState } from "react";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

const CheckoutCard = (props) => {
  const {handleOpen} = props;
  const [checkedItems, setCheckedItems] = useState({}); // State to track checked items
  const [selectAll, setSelectAll] = useState(false);
  const [totalPrice, setTotalPrice] = useState(0);
  const [checkboxItems, setCheckboxItems] = useState([
    {
      id: 1,
      img: "http://placekitten.com/1600",
      title: "SUV",
      name: "Hyundai Palisade 2021",
      schedule: "Wednesday,27 July 2021",
      price: 800000,
    },
    {
      id: 2,
      img: "http://placekitten.com/1600",
      title: "SUV",
      name: "Toyota Fortunner",
      schedule: "Sunday,24 July 2021",
      price: 850000,
    },
    {
      id: 3,
      img: "http://placekitten.com/1600",
      title: "SUV",
      name: "Course Suzuki XL7",
      schedule: "Wednesday,27 July 2021",
      price: 600000,
    },
    // Add more checkbox items as needed
  ]);
  const handleToggleAll = () => {
    const newCheckedItems = {};

    if (!selectAll) {
      checkboxItems.forEach((item) => {
        newCheckedItems[item.id] = true;
      });
    }

    setCheckedItems(newCheckedItems);
    setSelectAll(!selectAll);
  };

  const handleToggleSingle = (id) => () => {
    setCheckedItems({ ...checkedItems, [id]: !checkedItems[id] });
  };
  const calculateTotalPrice = () => {
    let total = 0;
    for (const id in checkedItems) {
      if (checkedItems[id]) {
        const selectedItem = checkboxItems.find(
          (item) => item.id === parseInt(id)
        );
        if (selectedItem) {
          total += selectedItem.price;
        }
      }
    }
    return total;
  };
  const handleRemove = (id) => {
    const updatedItems = checkboxItems.filter((item) => item.id !== id);
    setCheckboxItems(updatedItems);
  };
  return (
    <Grid item lg={12}>
      <Box px={2}>
        <Box
          borderBottom="2px solid #ccc"
          alignContent="flex-start"
          display="flex"
          paddingLeft="100px"
        >
          <FormControlLabel
            control={
              <Checkbox
                checked={selectAll}
                onChange={handleToggleAll}
                name="select-all"
                size="medium"
              />
            }
            label={
              <Typography variant="h5" marginLeft="10px">
                Pilih Semua
              </Typography>
            }
          />
        </Box>
        {checkboxItems.map((item) => (
          <Card style={{ width: "100%" }}>
            <Box
              borderBottom="2px solid #ccc"
              width="100%"
              display="flex"
              flexDirection="row"
              paddingLeft="100px"
              paddingRight="100px"
              paddingTop="20px"
              paddingBottom="20px"
            >
              <FormControlLabel
                key={item.id}
                control={
                  <Checkbox
                    size="medium"
                    checked={checkedItems[item.id] || false}
                    onChange={handleToggleSingle(item.id)}
                    name={item.name}
                  />
                }
                //label={item.label}
              />
              <CardMedia
                sx={{ width: 300, height: 200 }}
                image={item.img}
                title={item.title}
              />
              <CardContent>
                <Box
                  display="flex"
                  justifyContent="flex-start"
                  flexDirection="column"
                  alignItems="left"
                  marginLeft="50px"
                  width="75vw"
                >
                  <Typography
                    gutterBottom
                    variant="h5"
                    color="text.secondary"
                    align="left"
                  >
                    {item.title}
                  </Typography>
                  <Typography gutterBottom variant="h4" align="left">
                    {item.name}
                  </Typography>
                  <Box display="flex">
                    <Typography
                      gutterBottom
                      variant="h5"
                      color="text.secondary"
                      align="left"
                    >
                      Schedule:
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h5"
                      color="text.secondary"
                      align="left"
                    >
                      {" "}
                      {item.schedule}
                    </Typography>
                  </Box>
                  <Typography variant="h4" color="#790B0A" align="left">
                    {" "}
                    IDR {item.price}
                  </Typography>
                </Box>
              </CardContent>
              <Box display="flex" justifyContent="center" alignItems="center">
                <DeleteForeverIcon
                  style={{ fontSize: "60px" }}
                  color="error"
                  onClick={() => handleRemove(item.id)}
                />
              </Box>
            </Box>
          </Card>
        ))}
        <Box
          sx={{
            position: "relative",
            bottom: 0,
            width: "100%",
            textAlign: "center",
            minHeight: "8%",

          }}
          alignItems="center"
          borderTop="4px solid #ccc"
          width="100%"
          display="flex"
          flexDirection="row"
        >
          <Box display="flex" width="80vw">
            <Typography
              variant="h5"
              color="text.secondary"
              align="left"
              marginRight="50px"
            >
              Total Price
            </Typography>
            <Typography s variant="h4" color="#790B0A" align="left">
              IDR {calculateTotalPrice()}
            </Typography>
          </Box>
          <Button
            variant="contained"
            style={{ backgroundColor: "#790B0A", color: "#fff", width: "10%" }}
            onClick={()=>handleOpen()}
          >
            Pay Now
          </Button>
        </Box>
      </Box>
    </Grid>
  );
};

export default CheckoutCard;
