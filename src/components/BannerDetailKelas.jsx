import React from 'react'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const BannerDetailKelas = () => {
  const loremText = `
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed luctus euismod dui, ac placerat velit fermentum eget.
  Curabitur in mi eget ex lacinia varius vel vitae lectus. Integer sed felis non tortor vehicula rhoncus vel in quam.
  Praesent tincidunt turpis vitae elit euismod, vel eleifend erat hendrerit.
`;
  const [schedule, setSchedule] = React.useState('');

  const handleChange = (event) => {
    setSchedule(event.target.value);
  };
  return (
    <div>
    <Card style={{ width: '100%'  }} >
      <Box width='100%' display='flex' flexDirection='column' padding='100px'>
        <Box width='100%' display='flex' flexDirection='row'  >
    <CardMedia
        sx={{ width:350 ,height: 350 }}
        image="http://placekitten.com/1600"
        title="SUV"
      />
      <CardContent>
      <Box display='flex' justifyContent='flex-start' flexDirection='column' alignItems='left' marginLeft='50px' width='100%' >
      <Typography gutterBottom variant="h5" color="text.secondary" align="left" >
          SUV
        </Typography>
        <Typography variant="h3" align="left">
          Hyundai Palisade 2021
        </Typography>
        <Typography variant="h3" color="#790B0A" marginBottom='30px' align="left">
          IDR. 800.000
        </Typography>
        <Box sx={{ maxWidth: 400,marginBottom:'30px'}}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Select Schedule</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={schedule}
          label="Schedule"
          onChange={handleChange}
        >
          <MenuItem value="Monday,25 July 2022">Monday,25 July 2022</MenuItem>
          <MenuItem value="Tuesday,26 July 2022">Tuesday,26 July 2022</MenuItem>
          <MenuItem value="Wednesday,27 July 2022">Wednesday,27 July 2022</MenuItem>
          <MenuItem value="Thursday,28 July 2022">Thursday,28 July 2022</MenuItem>
          <MenuItem value="Friday,29 July 2022">Friday,29 July 2022</MenuItem>
          <MenuItem value="Saturday,30 July 2022">Saturday,30 July 2022</MenuItem>
        </Select>
      </FormControl>
    </Box>
    <Box display='flex'>
    <Button variant="text" size="large" sx={{
      border: '2px solid #790B0A',
                marginRight:'20px',
                width:"350px",
                height:"50px",
                color: "#790B0A",
                bgcolor:"white",
                ":hover":{
                color:"white",
                bgcolor:"#790B0A",
                },
                textTransform:"none",
                borderRadius: "10px",
                }}>
              Add To Chart
            </Button>
        
            <Button variant="contained" size="large" sx={{
                marginLeft:'20px',
                width:"350px",
                height:"50px",
                color: "white",
                bgcolor:"#790B0A",
                ":hover": {
                    color:"#790B0A",
                    bgcolor:"white",
                        },
                textTransform:"none",
                borderRadius: "10px",
                }}>
              Buy Now
            </Button>
    </Box>
        </Box>
      </CardContent>
      </Box>
      <Box display='flex' justifyContent='flex-start' flexDirection='column' alignItems='left'  width='90%' marginTop='50px' >
        <Typography variant="h4" align="left" marginBottom='20px'>
          Description
        </Typography>
        <Typography  variant="h6" color="text.secondary" align="left">
          {loremText}
        </Typography>
        <Typography  variant="h6" color="text.secondary" align="left">
          {loremText}
        </Typography>
        </Box>
      </Box>
    </Card>
    </div>
  )
}

export default BannerDetailKelas