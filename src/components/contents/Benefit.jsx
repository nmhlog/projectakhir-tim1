import React from "react";
import Typography from "@mui/material/Typography";
import { Box, Grid, Stack } from "@mui/material";
import { Image } from "@mui/icons-material";
export default function Benefit() {
  return (
    <>
    <Grid item lg={12}>
      <Stack
        flexDirection={"row"}
        useFlexGap
        justifyContent="center"
        alignItems="flex-start"
      >
        <Box sx={{ width: "50%" }}>
          <Typography
            variant="h3"
            sx={{
              color: "var(--primary, #790B0A)",
              fontFamily: '"Montserrat",sans-serif',
              fontSize: "40px",
              fontStyle: "normal",
              fontWeight: 600,
              lineHeight: "48.76px",
              mb: "2%",
            }}
          >
            Gets your best benefit
          </Typography>
          <Typography
            variant="h3"
            sx={{
              color: "var(--primary, #333333)",
              fontFamily: '"Montserrat",sans-serif',
              fontSize: "16px",
              fontStyle: "normal",
              fontWeight: 500,
              lineHeight: "19.5px",
              textAlign:"justify",
              mb: "2%",
            }}
          >
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
            quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
            aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
            eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
            qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
            sed quia non numquam.
          </Typography>
          <Typography
            variant="h3"
            sx={{
              color: "var(--primary, #333333)",
              fontFamily: '"Montserrat",sans-serif',
              fontSize: "16px",
              fontStyle: "normal",
              fontWeight: 500,
              textAlign:"justify",
              lineHeight: "19.5px",
              mb: "2%",
            }}
          >
            Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
            consectetur, adipisci velit, sed quia non numquam eius modi tempora
            incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
          </Typography>
        </Box>
        <Box
          sx={{
            width: "40%",
            backgroundImage: 'url("Image/Benefits_background.png")',
            backgroundRepeat: "no-repeat",
            transform: "rotate(180deg)",
            // pr:"0px",
            // pl:"50px"
          }}
        >
          <img
            src="http://localhost:3000/Image/Benefits_person.png"
            alt="Benefit"
            style={{
              transform: "rotate(180deg)",
              display:"block",
              margin: "0px auto",
            //   padding: "9px auto",
            }}
          ></img>
        </Box>
      </Stack>
      </Grid>
    </>
  );
}
