import { Stack,Grid,Typography, Box } from "@mui/material";
import CarCard from "../CarCard";


export default function ListCarCard(props){
    const {data,loading} = props;


    return (
        <>
        <Grid item lg={12}>
        {loading?null:(<Stack
        flexDirection={"column"}
        useFlexGap
        justifyContent="center"
        alignItems="center"
      >
        <Typography
          variant="h4"
          sx={{
            color: "var(--primary, #790B0A)",
            fontFamily: '"Montserrat",sans-serif',
            fontSize: "32",
            fontStyle: "normal",
            fontWeight: 600,
            lineHeight: "38.73px",
            textAlign:"center",
            mb:"6%",
          }}
        >
          Join us for the course
        </Typography>
        <Box sx={{ml:"8%",mr:"5%"}}>

        <Stack direction={"row"} gap={2} useFlexGap flexWrap={"wrap"} >

        {
            data.map((item)=>
                
                <CarCard 
                id={item.id}
                name = {item.name}
                src = {item.source}
                typeCar = {item.typeCar}
                price = {item.price}
                />
            )
        }
        </Stack>
        </Box>
        </Stack>)}

        </Grid>

        </>
    );
}