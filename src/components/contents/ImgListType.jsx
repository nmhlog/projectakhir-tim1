import React from "react";
import {
  ImageList,
  ImageListItem,
  ImageListItemBar,
  Grid,
  Typography,
} from "@mui/material";
/*
*TODO:
NavLink to menuclass,


*/

export default function ImgListType({typeData,loading}) {
  
  

  return (
    <>
      <Grid item lg={12} >
        {loading?null:(
        <Typography
          variant="h4"
          sx={{
            color: "var(--primary, #790B0A)",
            fontFamily: '"Montserrat",sans-serif',
            fontSize: "32",
            fontStyle: "normal",
            fontWeight: 600,
            lineHeight: "38.73px",
            mb:"6%",
            textAlign:"center",
          }}
        >
          More car type you can choose
        </Typography>)}
        <ImageList
          sx={{
            margin: "auto",
            width: "80%",
            display: "flex",
            flexWrap: "wrap",
            gap: "10%",
            justifyContent: "center",
          }}
        >
          {loading?null:(typeData.map((item) => (
            <ImageListItem
              key={item.id}
              sx={{ width: "200px", height: "120px" }}
            >
              <img
                key={item.id}
                src={item.source}
                alt={item.type_name}
                loading="lazy"
                style={{ width: "100px", height: "67px", margin: "auto" }}
              />
              <ImageListItemBar
                title={item.type_name}
                position="below"
                sx={{ textAlign: "center" }}
              />
            </ImageListItem>
          )))}
        </ImageList>
      </Grid>
    </>
  );
}
