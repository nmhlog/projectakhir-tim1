import React from "react";
import Success from "../logos/Success";
import TextGroup from "../textcostum/TextGroup";
import { Box, Grid, Stack,Typography, Button } from "@mui/material";
import { NavLink } from "react-router-dom";
import ButtonCore from "../buttoncustom/ButtonCore";



export default function ConfirmSuccess(){

    return (
        <Grid item xs={12} >
            <Stack direction='column' alignItems="center" spacing={{ xs: 1, sm: 2, md: 4 }}>
            <Success />
            <TextGroup 
                mainName={"Email Confirmation Success"}
                SupportName={"Your email already! Please login first to access the web"}
                textAlign="center"
            />
            <ButtonCore 
            NavTo={"/login"}
            name={"Login"}
            />
            
            </Stack>
        </Grid>
    )
}