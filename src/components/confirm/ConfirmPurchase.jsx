import React from "react";
import Success from "../logos/Success";
import TextGroup from "../textcostum/TextGroup";
import { Grid, Stack } from "@mui/material";

import ButtonCore from "../buttoncustom/ButtonCore";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import HomeIcon from "@mui/icons-material/Home";

export default function ConfirmPurchase() {
  return (
    <Grid item xs={12}>
      <Stack
        direction="column"
        alignItems="center"
        spacing={{ xs: 1, sm: 2, md: 4 }}
      >
        <Success />
        <TextGroup
          mainName={"Purchase Successfully"}
          SupportName={"That’s Great! We’re ready for driving day"}
          textAlign="center"
        />
        <Stack direction={"row"} gap={2}>
          <ButtonCore
            NavTo={"/"}
            name={"Back to Home"}
            isReverse={true}
            isBorder={true}
          >
            <HomeIcon size="small" sx={{mr:"10px"}}/> Back to Home
          </ButtonCore>
          <ButtonCore NavTo={"/invoice"} name={"Open Invoice"}>
            <ArrowForwardIcon size="small" sx={{mr:"10px"}}/> Open Invoice{" "}
          </ButtonCore>
        </Stack>
      </Stack>
    </Grid>
  );
}
