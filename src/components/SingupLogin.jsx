import React from "react";
import { NavLink } from "react-router-dom";
import { Button,Stack } from "@mui/material";

export default function SingupLogin({pathname}){


    return (
        <Stack direction="row" spacing={2}>

        
        <Button variant="text" size="medium" sx={{
                px:"20px",
                py:"5px",
                color: "#790B0A",
                bgcolor:"white",
                ":hover":{
                color:"white",
                bgcolor:"#790B0A",
                },
                textTransform:"none",
                borderRadius: "10px",
                }}>
              Sign Up
            </Button>
        
            <Button variant="contained" size="small" sx={{
                px:"20px",
                py:"5px",
                color: "white",
                bgcolor:"#790B0A",
                ":hover": {
                    color:"#790B0A",
                    bgcolor:"white",
                        },
                textTransform:"none",
                borderRadius: "10px",
                }}>
              Login
            </Button>
          
        </Stack>
    )
}
