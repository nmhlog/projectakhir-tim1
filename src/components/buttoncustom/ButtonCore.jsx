import React from "react";
import { NavLink } from "react-router-dom";
import { Button } from "@mui/material";

function ButtonCore(props) {
  const { NavTo, name, isReverse, isBorder, size, pxsize, pysize, onclick } =
    props;
  const px = pxsize ? pxsize : "20px";
  const py = pysize ? pysize : "5px";

  const handleOnClick = () => {
    if (onclick) {
      return ()=>onclick();
    }
  };
  return (
    <NavLink to={NavTo}>
      <Button
        variant="text"
        size={size ? "Large" : "medium"}
        sx={{
          fontFamily: '"Montserrat",sans-serif',
          px: px,
          py: py,
          color: isReverse ? "#790B0A" : "white",
          bgcolor: isReverse ? "white" : "#790B0A",
          ":hover": {
            color: isReverse ? "white" : "#790B0A",
            bgcolor: isReverse ? "#790B0A" : "white",
          },
          textTransform: "none",
          borderRadius: "10px",
          border: isBorder ? "1px solid #790B0A" : null,
        }}
        onClick={handleOnClick()}
      >
        {props.children ? props.children : name}
      </Button>
    </NavLink>
  );
}

export default ButtonCore;
