import React from "react";
import { Stack, Divider } from "@mui/material";
import ButtonCore from "../buttoncustom/ButtonCore";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PersonIcon from "@mui/icons-material/Person";
import LogoutIcon from "@mui/icons-material/Logout";
export default function LoginButtonGroup() {
  return (
    <Stack
      flexDirection={"row"}
      gap={2}
      sx={{ justifyContent: "flex-end", alignItems: "center" }}
    >
      <ShoppingCartIcon sx={{ color: "#790B0A" }} />
      <ButtonCore NavTo={"/"} name={"MyClass"} isReverse={true}></ButtonCore>
      <ButtonCore NavTo={"/invoice"} name={"Invoice"} isReverse={true}></ButtonCore>
      <Divider orientation="vertical" flexItem sx={{ color: "#790B0A" }} />
      <PersonIcon sx={{ color: "#790B0A" }} />
      <LogoutIcon sx={{ color: "#790B0A" }} />
    </Stack>
  );
}
