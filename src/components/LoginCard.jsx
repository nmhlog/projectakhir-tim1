import React, { useState } from 'react';
import { Card, CardContent, Typography, TextField, Button, Box } from '@mui/material';
import { NavLink } from 'react-router-dom';
const LoginCard = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    console.log('Email:', email);
    console.log('Password:', password);
  };

  return (
    <div style={{display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh'}} >
    <Card style={{width: '50%'}}>
      <CardContent>
        <Box  display='flex' justifyContent='flex-start'>
        <Typography variant='h3' color='#790B0A' >Welcome Back!</Typography>
        </Box>
        <Box display='flex' justifyContent='flex-start' marginBottom='30px'>
        <Typography variant='h5' color='grey' >Please Login First</Typography>
        </Box>
        <form>
          <TextField
            label="Email"
            variant="outlined"
            fullWidth
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            margin="normal"
          />
          <TextField
            label="Password"
            variant="outlined"
            fullWidth
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password"
            margin="normal"
          />
          <Box  display='flex' justifyContent='flex-start' marginTop='20px' marginBottom='20px'>
        <Typography variant='h7' color='grey'>Forgot Password ? <NavLink to={"/forgetpassword"} sx={{marginLeft:"20px"}}>Click Here</NavLink></Typography>
        </Box>
        <Box  display='flex' justifyContent='flex-end' marginBottom='40px'>
          <Button  variant='contained' style={{ backgroundColor: '#790B0A', color: '#fff',width:'10%'}} onClick={handleLogin}>
            Login
          </Button>
          </Box>
        </form>
        <Box  display='flex' justifyContent='center' marginTop='20px' marginBottom='20px'>
        <Typography variant='h7' color='grey'>Dont have account? <NavLink to={"/signup"} sx={{marginLeft:"10px"}}>Sign up here</NavLink></Typography>

        </Box>
      </CardContent>
    </Card>
    </div>
  );
};

export default LoginCard;
