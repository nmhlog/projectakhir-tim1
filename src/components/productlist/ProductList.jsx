import React from "react";
import { Box, ListItemText, Stack, Typography } from "@mui/material";
import { List, ListItem } from "@mui/material";
/*
*TODO:
ProductDummy from usedContext
*/

export default function ProductList() {
  const productDummy = [
    "Electric",
    "LCGC",
    "Offroad",
    "SUV",
    "Hatchback",
    "MPV",
    "Sedan",
    "Truck",
  ];
  return (
    <>
      <Stack
        direction={"column"}
        sx={{
          // border: "2px solid black",
          alignContent:"center",
          width:"20%"
        }}
      >
        <Typography
          variant="h6"
          color="#790B0A"
          sx={{
            fontFamily: "'Poppins', sans-serif",
            fontWeight: "500",
            size: "16px",
            lineHeight: "24px",
            textAlign: "justify",
          }}
        >
          Product
        </Typography>
        <List
          // dense
          container
          sx={{
            overflow: "hidden",
            display: "flex",
            flexFlow: "column",
            flexWrap: "wrap",
            pl:2,
            width:"228px",
            height:"108px",
            // border:"2x solid red"
          }}
        >
          {productDummy.map((e) => (
            <ListItem sx={{ 
                          display: "list-item",
                          listStyleType:"disc", 
                          width:"10%",
                          height:"25%",
                          p:"2px",
                          mt:0
                        }}>
              <ListItemText primary={<Typography variant="body2" style={{
                fontFamily:"'Poppins', sans-serif",
                lineHeight:"21px",
                textAlign:"justify",
                color: '#333333',
                fontWeight:"400",
                fontSize:"14px",
                }}>{e}</Typography>}></ListItemText>
            </ListItem>
          ))}
        </List>
      </Stack>
    </>
  );
}
