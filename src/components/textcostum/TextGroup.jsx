import React from "react";
import { Typography,Box } from "@mui/material";

export default function TextGroup({mainName,SupportName,textAlign}){
    return (
        <Box sx={{pt:"2%"}}>
            <Typography
                variant="h1"
                component="h1"
                sx={{
                  color: "var(--primary, #790B0A)",
                  fontFamily: '"Montserrat",sans-serif',
                  fontSize: "24px",
                  fontStyle: "normal",
                  fontWeight: 500,
                  lineHeight: "normal",
                  textAlign: textAlign?textAlign:"center",
                  mb:"2%"
                }}
              >
                {mainName}
              </Typography>
              <Typography
                sx={{
                  color: "var(--primary, #4F4F4F)",
                  fontFamily: '"Montserrat",sans-serif',
                  fontSize: "16px",
                  fontStyle: "normal",
                  fontWeight: 400,
                  lineHeight: "normal",
                  textAlign: textAlign?textAlign:"center",
                }}
              >
                {SupportName}
              </Typography>
        </Box>
    )

}