import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material';

export default function CarCard(props) {
  const {id,name,src,price,typeCar} = props;
  return (
    <Card id={id} sx={{ minWidth: 345 }} >
      <CardMedia
        sx={{ height: 250, width:"100%"}}
        image= {src}
        title={typeCar}
      />
      <CardContent>
        <Box display='flex' justifyContent='flex-start' flexDirection='column' alignItems='left'>
        <Typography gutterBottom variant="body1" color="text.secondary" align="left" >
          SUV
        </Typography>
        <Typography variant="h5" align="left">
          {name}
        </Typography>
        <Typography variant="h5" color="#790B0A" marginTop='30px' align="left">
          {price}
        </Typography>
        </Box>
      </CardContent> 
    </Card>
  );
}